**Project to Crawl the Burberry Website for items.**


Example output (items.json):

![image](https://bitbucket.org/soniascode/scrapy-project/raw/249c22d2dfad06d08180b67078d8a7ac8c4ee801/example.png)


Run project:

	$ cd project_folder
	$ scrapy crawl burberry -o items.json -t json
