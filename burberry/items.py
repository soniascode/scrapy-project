from scrapy.item import Item, Field


class BurberryItem(Item):

    department = Field()
    collection = Field()
    category = Field()

    name = Field()
    price = Field()

