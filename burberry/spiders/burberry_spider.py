from collections import defaultdict
import re

from scrapy.http import Request
from scrapy.selector import Selector
from scrapy.spider import Spider

from burberry.items import BurberryItem


class BurberrySpider(Spider):
    """
    Info:
        - each start url represents a Burberry Department
        - each Department is composed from a list of Collections
        - each Collection is composed from a list of Categories
        - each Category is composed from a list of Items

    Each Department has a sublist of URLs representing different Collections.
        For example the "Children" Department is divided into four Collections:
        "Boy", "Girl", "Baby", "Children Gifts".

    Each Collection page is divided into categories of that type of product.
        For example the "Sandal" Collection are divided into three Categories:
        "Heeled", "Flat" and "Espadrilles".

    Each Category has items -- which will be delivered in the final items.json.
    """

    name = 'burberry'
    main_url = 'http://ro.burberry.com'
    allowed_domains = ["ro.burberry.com"]
    start_urls = [
        "http://ro.burberry.com/women",
        "http://ro.burberry.com/men",
        "http://ro.burberry.com/children"]

    def parse(self, response):
        """Parse a Department and extract its coresponding Collections. Then
        make a request to the URL of each Collection.

        Returns:
            list of dictionaries (all the items in the Department)
        """

        # Get the Collections.
        sel = Selector(response)
        menu_xpath = ('//ul/li[@class="l-1-link l-1-link-open"]'
                      '/div/ul/li/div/ul/li/a/@href')
        menu_list = sel.xpath(menu_xpath).extract()

        # Make a request to each Collection.
        for collection in menu_list:
            url = '%s/%s' % (self.main_url, collection)
            req = Request(url, callback=self.parse_collection)
            req.meta['department'] = response.url.split('/')[-1].title()
            req.meta['collection'] = collection
            yield req

    def parse_collection(self, response):
        """Parse a Collection and extract its coresponding Categories. Then
        parse the selectors for each Category.

        Returns:
            list of dictionaries (all the items in the Collection)
        """

        # Get the item's properties from the Request.
        department = response.meta['department']
        collection = response.meta['collection']
        collection = re.sub('[\W]', ' ', collection).strip().title()

        # Get the Collections.
        page_selector = Selector(response)
        categories_selectors = page_selector.xpath('//ul[@class="product-set"]')

        # Parse each Collection.
        results = []
        for sel in categories_selectors:
            category = sel.xpath('@data-track-name').extract()[0]
            results += self.parse_category(sel, department, collection, category)
        return results

    def parse_category(self, sel, department, collection, category):
        """Parse a Category and extract the items.

        Returns:
            list of dictionaries (all items in the Category)
        """

        # Get all the items' prices and names.
        names = sel.xpath('li/div/div/div/text()')
        prices = sel.xpath('li/div/div/p/span/text()').re('\d*,*\d+\..+')

        # Avoid duplicates. A duplicate is an item with the same name,
        # price and category as another but in a different color.
        results = defaultdict(BurberryItem)

        for i in range(len(names)):
            name = names[i].extract()

            if name not in results:
                new_item = BurberryItem(name=name,
                                        price='$%s' % prices[i],
                                        department=department,
                                        collection=collection,
                                        category=category)
                results[name] = new_item

        # Return the unique items.
        return results.values()
